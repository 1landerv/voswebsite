import React, {Fragment, useState, useEffect} from 'react';
import './App.css';
import Navbar from './components/navbar';
import Navmenu from './components/navmenu';
import Login from './components/login';
import Instructionlist from './components/instructionlist';
import { makeStyles } from '@material-ui/core/styles';
import { Paper } from '@material-ui/core';
import { BrowserRouter as Router, Switch, Route} from "react-router-dom";

import procedureListLDO from './data/procedureListLDO.json';

const useStyles = makeStyles(() => ({
  paper: {
    padding: 10,
    marginTop: 20,
    marginLeft: 150,
    marginRight: 150,
    backgroundColor: "#dedcdc"
  },
  paper2: {
    paddingTop: 15,
    paddingBottom: 15,
    marginTop: 20,
    marginLeft: 150,
    marginRight: 150,
    backgroundColor: "#ffffff"
  }
}));

function App() {
  const classes = useStyles();
  const [location, setLocation] = useState({latitude: 0, longitude: 0});
  
  useEffect(() => {
    navigator.geolocation.getCurrentPosition((position) => {
    setLocation({latitude: position.coords.latitude, longitude: position.coords.longitude})
    });
    console.log(location);
  },[location.latitude,location.longitude]);

    return (
    <Fragment>
      <Navbar/>
      <Paper className={classes.paper} square>
        <Router>
          <Switch>
            <Route exact path="/">
              <Navmenu/>
            </Route>
            <Route exact path="/login">
              <Login/>
            </Route>
            <Route exact path="/brand">
              <Instructionlist procedure={procedureListLDO.procedure[0]}/>
            </Route>
            <Route exact path="/gif">
              <Instructionlist procedure={procedureListLDO.procedure[1]}/>
            </Route>
            <Route exact path="/bommelding">
              <Instructionlist procedure={procedureListLDO.procedure[2]}/>
            </Route>
          </Switch>
        </Router>
      </Paper>
    </Fragment>
  );
}

export default App;
