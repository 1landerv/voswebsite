import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import { Link } from "react-router-dom";

const useStyles = makeStyles({
  root: {
    paddingLeft: '80px',
    paddingRight: '80px',
    paddingTop: '20px',
    paddingBottom: '20px',
    fontSize: 30,
    backgroundColor: props => props.color,
  }
});

export default function Navbutton(props) {
  const classes = useStyles(props);
  return (
    <Link
      style={{textDecoration: 'none'}}
      to={{pathname: props.route}}
    >
    <Button 
      variant="contained" 
      classes={{root: classes.root}}
    >{props.title}</Button>
    </Link>
  );
}